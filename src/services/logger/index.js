import logger, { transports } from 'winston';
import winstonExpress from 'express-winston';

logger.configure({
  // format: format.combine(format.timestamp(), format.simple()),
  transports: [
    new transports.Console({
      level: process.env.LOG_LEVEL || 'info',
      colorize: true,
      timestamp: true
      /* format: format.combine(
				format.timestamp(),
				format.colorize(),
				format.metadata(),
				format.printf((info, opt) => {
					return JSON.stringify(opt);
					return `${info.metadata.timestamp} ${info.level}: ${
						info.message
					}. [${JSON.stringify(info.meta)}]`;
				})
			) */
    })
  ]
});

export const expressLogger = winstonExpress.logger({
  winstonInstance: logger, // TODO: log errors explicitly
  msg:
    '{{req.method}} {{res.statusCode}} {{res.responseTime}}ms user:{{req.user?req.user._id:"Anonymous"}} {{req.url.split("?")[0]}}',
  colorize: true
});

export const expressDebugLogger = winstonExpress.logger({
  winstonInstance: logger, // TODO: log errors explicitly
  msg:
    '{{req.method}} {{res.statusCode}} {{res.responseTime}}ms user:{{req.user?req.user._id:"Anonymous"}} {{req.url.split("?")[0]}} body:{{JSON.stringify(req.body)}}',
  level: 'debug',
  colorize: true
});

export const expressErrorLogger = winstonExpress.errorLogger({
  winstonInstance: logger, // TODO: log errors explicitly
  msg:
    '{{req.method}} {{res.statusCode}} {{res.responseTime}}ms user:{{req.user?req.user._id:"Anonymous"}} {{req.url.split("?")[0]}} body:{{JSON.stringify(req.body)}}',
  colorize: true,
  meta: false
});

export default logger;
