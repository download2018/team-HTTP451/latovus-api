import { setupPushNotificationsService } from '@becodebg/odin-api-pushnotifications';

import { model } from '../../api/devices';
import { pushNotifications as pushNotificationsSettings } from '../../config';

export const PushNotificationService = setupPushNotificationsService(
  model,
  pushNotificationsSettings.serverKey,
  pushNotificationsSettings.topic
);
