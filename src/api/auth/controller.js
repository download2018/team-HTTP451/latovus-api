import { success } from '@becodebg/odin-services-response';
import Promise from 'bluebird';

import { sign } from '../../services/jwt';

const actions = {};

actions.login = ({ user }, res, next) => Promise.all([sign(user.id), user.view(true, null, { populate: true })])
    .then(([token, userView]) => ({
      token,
      user: userView
    }))
    .then(success(res, 201))
    .catch(next);

actions.checkJWT = (req, res) => res.sendStatus(200);

export default actions;
