import { ModelGenerator, getValidationSchema } from '@becodebg/odin-generators';
import logger from 'winston';
import mongoose, { Schema } from 'mongoose';

import { appName } from '../../config';

const notificationTypes = [''];

let mongooseSchema =
  /**
   * @api {js} notification Schema
   * @apiGroup Notifications
   * @apiName Schema
   * @apiExample {js} Entity schema: */
  {
    created_by: {
      type: Schema.ObjectId,
      ref: 'User'
    },
    targetUser: {
      type: Schema.ObjectId,
      ref: 'User'
    },
    sent: {
      type: Boolean,
      default: false
    },
    event: {
      type: String,
      enum: notificationTypes
    }
  };
/* **/

const model = ModelGenerator(mongoose)({
  schema: mongooseSchema,
  collectionName: 'notifications',
  modelName: 'Notification',
  populationOptions: [
    {
      path: 'created_by',
      select: '-password'
    },
    {
      path: 'targetUser',
      select: '-password'
    }
  ],
  extensionFunction: schema => {
    schema.statics.getUnsentNotifications = function() {
      return this.find({ sent: false }).catch(e => {
        logger.error(e);
        throw e;
      });
    };

    schema.methods.getMessage = function() {
      return this.view().then(element => {
        switch (element.event) {
          default:
            return {
              title: appName,
              body: 'You have new notifications'
            };
        }
      });
    };

    schema.methods.markSent = function() {
      return this.view().then(() => {
        this.sent = true;
        return this.save();
      });
    };
  }
});

export const schema = model.schema;
export const bodymenSchema = getValidationSchema(mongooseSchema);

export default model;
