import { Router } from 'express';
import { middleware as body } from 'bodymen';
import { middleware as query } from 'querymen';

import { token } from '../../services/passport';

import { actions } from './controller';
import { bodymenSchema } from './model';

const router = new Router();

/**
 * @api {get} /tickets List tickets
 * @apiGroup Ticket
 * @apiName ListTickets
 * @apiPermission user
 * @apiUse listParams
 * @apiSuccess {Ticket[]} tickets List of Tickets.
 **/
router.get(
  '/',
  token({ required: true }),
  query(bodymenSchema.query),
  actions.index
);

/**
 * @api {get} /tickets/stats List tickets stats
 * @apiGroup Ticket
 * @apiName ListTicketsStats
 * @apiPermission user
 * @apiUse listParams
 * @apiSuccess {Ticket[]} tickets List of Tickets stats.
 **/
router.get(
  '/stats',
  token({ required: true }),
  query(bodymenSchema.query),
  actions.stats
);

/**
 * @api {get} /tickets/:id Retrieve ticket
 * @apiGroup Ticket
 * @apiName RetrieveTicket
 * @apiPermission user
 * @apiSuccess {Ticket} ticket Ticket's data.
 * @apiError 404 Ticket not found.
 **/
router.get('/:id', token({ required: true }), actions.show);

/**
 * @api {post} /tickets Create ticket
 * @apiGroup Ticket
 * @apiName CreateTicket
 * @apiPermission user
 * @apiSuccess (Success 201) {Ticket} ticket new Ticket's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 **/
router.post(
  '/',
  token({ required: true }),
  body(bodymenSchema.creation),
  actions.create
);

/**
 * @api {put} /tickets/:id Update ticket
 * @apiGroup Ticket
 * @apiName UpdateTicket
 * @apiPermission user
 * @apiSuccess {Ticket} ticket Ticket's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Ticket not found.
 **/
router.put(
  '/:id',
  token({ required: true }),
  body(bodymenSchema.update),
  actions.update
);

/**
 * @api {delete} /tickets/:id Delete Ticket
 * @apiGroup Ticket
 * @apiName DeleteTicket
 * @apiPermission user
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Ticket not found.
 **/
router.delete('/:id', token({ required: true }), actions.destroy);

export Entity from './model';

export default router;
