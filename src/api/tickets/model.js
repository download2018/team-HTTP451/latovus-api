import { getValidationSchema, ModelGenerator } from '@becodebg/odin-generators';
import mongoose, { Schema } from 'mongoose';

const mongooseSchema =
  /**
   * @api {js} tickets Schema
   * @apiGroup Ticket
   * @apiName Schema
   * @apiExample {js} Entity schema: */
  {
    issueIds: {
      type: [Schema.ObjectId],
      odinVirtualPopulation: {
        odinAutoPopulation: true,
        fieldName: 'issues',
        options: {
          ref: 'Issue'
        }
      }
    },
    status: {
      type: String,
      enum: ['Nuovo', 'Preso in carico', 'Risolto']
    },
    description: { type: String },
    title: { type: String },
    executor: { type: String },
    executionDate: { type: Date },
    department: { type: String },
    note: { type: String },
    startDate: { type: Date },
    endDate: { type: Date }
  };
/* **/

const model = ModelGenerator(mongoose)({
  schema: mongooseSchema,
  collectionName: 'tickets',
  modelName: 'Ticket',
  populationOptions: []
});

export const schema = model.schema;
export const bodymenSchema = getValidationSchema(mongooseSchema);

export default model;
