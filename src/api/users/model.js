import { ModelGenerator, getValidationSchema } from '@becodebg/odin-generators';
import bcrypt from 'bcrypt';
import Promise from 'bluebird';
import mongoose, { Schema } from 'mongoose';

import { env } from '../../config';

const roles = ['app-user', 'admin', 'operatore'];

let mongooseSchema =
/**
 * @api {js} users Schema
 * @apiName Schema
 * @apiGroup User
 * @apiExample {js} Entity schema: */
{
  email: {
    type: String,
    match: /^\S+@\S+\.\S+$/,
    required: true,
    unique: true,
    trim: true,
    lowercase: true,
    odinFrozenAfterCreation: true
  },
  password: {
    type: String,
    required: true,
    minlength: 6
  },
  role: {
    type: String,
    enum: roles,
    default: 'app-user',
    odinFrozenAfterCreation: true
  },
  pictureId: {
    type: Schema.ObjectId
  },
  isConfirmed: {
    type: Boolean,
    default: false
  },
  experiencePoints: {
    type: Number
  },
  badges: {
    type: [String] // tipo achivement per l'utente
  },
  name: {
    type: String
  },
  facebookId: {
    type: String
  },
  department: {
    type: String,
    enum: [
      'Lavori pubblici',
      'Innovazione',
      'Sport',
      'Turismo',
      'Politiche per lo sviluppo',
      'Pianificazione urbanistica'
    ]
  },
  last_login: {
    // last real login
    type: Date
  },
  pre_last_login: {
    // last login showed to the user
    type: Date
  }
};
/* **/

const model = ModelGenerator(mongoose)({
  schema: mongooseSchema,
  collectionName: 'users',
  modelName: 'User',
  populationOptions: [{ path: 'picture' }],
  extensionFunction: schema => {
    schema.virtual('picture', {
      ref: 'UploadedFile',
      foreignField: '_id',
      localField: 'pictureId',
      justOne: true
    });

    schema.virtual('devices', {
      ref: 'Device',
      foreignField: 'userId',
      localField: '_id',
      justOne: false
    });

    schema.pre('save', function (next) {
      if (!this.isModified('password')) return next();

      /* istanbul ignore next */
      const rounds = env === 'test' ? 1 : 9;

      bcrypt
        .hash(this.password, rounds)
        .then(hash => {
          this.password = hash;
          next();
        })
        .catch(next);
    });

    schema.methods.authenticate = function (password) {
      return Promise.resolve(bcrypt.compare(password, this.password))
        .bind(this)
        .then(valid => (valid ? this : false));
    };

    schema.statics = {
      roles
    };
  }
});

export const schema = model.schema;
export const bodymenSchema = getValidationSchema(mongooseSchema);

export default model;
