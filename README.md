# La tò us

API per la pubblica amministrazione per il ticketing cittadino-comune 

## Getting Started

Il progetto è sviluppato utilizzando lo stack Mongo-Express-Node e fornisce delle API rest in grado di integrarsi con sistemi esterni. La documentazione è consultabile a nella cartella [docs](docs) all'interno della codebase.

NB: la documentazione verrà rigenerata ed eventualmente aggiornata dopo l'esecuzione di `npm install`. È stata inclusa per comodità.

### Prerequisites

Per effettuare l'installazione dell'applicazione è necessario avere installato una versione di node successiva alla 7 e la disponibilità di un database Mongo.

### Running

Per eseguire l'applicazione sarà necessario eseguire `npm start`

### Deploy

L'applicazione è studiate per girare nel cloud utilizzando `Heroku` (API e DB) e `AWS` (hosting risorse statiche ed eventuali frontend). Per automatizzare il deploy è stato allegato uno script `Terraform`. Per eseguirlo (dopo aver scaricato l'eseguibile e aver compilato le variabili nel file `tfvars`) eseguire `terraform init` e `terraform apply`.

### BOT
[BOT](chatbot/laToUs_Chatbot.txt)