# This is the name of the Heroku application that we will create
# This needs to be unique (no to accounts can have the same name)
variable "app_name" {
  description = "Name of the application that will be created."
}

# Application environment - usually something like 'dev' or 'prod'
# We use this to label items and generate names
variable "app_environment" {
  type        = "string"
  default     = "dev"
  description = "Application environment - usually something like 'dev' or 'prod'. Used to label items and generate names"
}

#
# AWS settings
#

# Authentication for the AWS provider - we use this access
# key in order to be able to create a new AWS user and the S3
# bucket that the application needs.
variable "aws_access_key_id" {}

variable "aws_access_secret_key" {}
