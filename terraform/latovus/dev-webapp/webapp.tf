module "s3website" {
  source = "../../../modules/s3website"

  # AWS access
  aws_access_key_id     = "${var.aws_access_key_id}"
  aws_access_secret_key = "${var.aws_access_secret_key}"

  # S3 setup
  s3_bucket_name              = "${var.app_name}-${var.app_environment}-webapp"
  s3_resource_tag_name        = "${var.app_name}"
  s3_resource_tag_environment = "${var.app_environment}"

  # CloudFront setup
  cloudfront_cnames = ["sample.runelab.it"]
}