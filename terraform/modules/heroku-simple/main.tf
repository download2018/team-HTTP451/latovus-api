#
# HEROKU
#

# We need the Heroku provider in order to create the Heroku application
provider "heroku" {
  email   = "${var.email}"
  api_key = "${var.api_key}"
}

# Creates random secrets
resource "random_string" "app_jwt_secret_seed" {
  length = 128
}

resource "random_string" "app_master_token" {
  length = 128
}

# Creates the primary Heroku application.
resource "heroku_app" "default" {
  name   = "${var.app_name}"
  region = "${var.app_region}"

  # We do need a URL for the database, but we don't
  # need to create it because provisioning the
  # addon automatically created this config var
  config_vars = {
    # App specific
    APP_NAME = "${var.app_name}"

    JWT_SECRET = "${sha512(random_string.app_jwt_secret_seed.result)}"
    MASTER_KEY = "${sha512(random_string.app_master_token.result)}"

    SENDGRID_KEY = "${var.app_sendgrid_key}"
  }

  buildpacks = [
    "heroku/nodejs",
  ]
}

# Create the Heroku MongoDB addon for the application
resource "heroku_addon" "mongodb" {
  app  = "${heroku_app.default.name}"
  plan = "${var.db_plan}"
}

# Create the Heroku logger addon for the application
resource "heroku_addon" "logger" {
  app  = "${heroku_app.default.name}"
  plan = "${var.logger_plan}"
}

# OUTPUTS
output "mongodb_url" {
  value = "${heroku_app.default.all_config_vars["MONGODB_URI"]}"
}
