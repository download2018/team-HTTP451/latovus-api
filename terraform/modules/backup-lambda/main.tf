#
# AWS and Lambda
#

# We need the AWS provider in order to create the S3 bucket
provider "aws" {
  access_key = "${var.aws_access_key_id}"
  secret_key = "${var.aws_access_secret_key}"
  region     = "${var.aws_region}"
}

resource "aws_lambda_function" "mongo_backup_lambda" {
  s3_bucket     = "lambda-runelab-source"
  s3_key        = "lambda-mongodb-s3-backup.zip"
  function_name = "lambda-mongodb-s3-backup-${var.app_name}"
  role          = "arn:aws:iam::972391560074:role/lambda-mongodb-s3-backup"
  handler       = "index.handler"
  runtime       = "nodejs8.10"
  timeout       = 300

  environment {
    variables = {
      MONGO_URL = "${var.mongodb_url}"
      S3_PATH   = "runelab-mongodump/${var.app_name}"
    }
  }
}

resource "aws_cloudwatch_event_target" "backup_lambda" {
  rule = "everydayAtMidnight"
  arn  = "${aws_lambda_function.mongo_backup_lambda.arn}"
}

resource "aws_lambda_permission" "backup_lambda_every_midnight" {
  statement_id  = "AllowExecutionFromCloudWatch"
  action        = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.mongo_backup_lambda.function_name}"
  principal     = "events.amazonaws.com"
  source_arn    = "arn:aws:events:eu-west-1:972391560074:rule/everydayAtMidnight"
}
