define({ "api": [
  {
    "type": "post",
    "url": "/auth",
    "title": "Authenticate",
    "name": "Authenticate",
    "group": "Auth",
    "permission": [
      {
        "name": "basic"
      }
    ],
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>Basic authorization with email and password.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 201": [
          {
            "group": "Success 201",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>User <code>access_token</code> to be passed to other requests.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401",
            "description": "<p>Master access only or invalid credentials.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/api/auth/index.js",
    "groupTitle": "Auth"
  },
  {
    "type": "get",
    "url": "/checkJWT",
    "title": "Check JWT Validity",
    "name": "Check_JWT_Validity",
    "group": "Auth",
    "permission": [
      {
        "name": "token"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "access_token",
            "description": "<p>access_token.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "OK",
            "description": ""
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401",
            "description": "<p>Master access only or invalid credentials.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/api/auth/index.js",
    "groupTitle": "Auth"
  },
  {
    "type": "get",
    "url": "/renewJWT",
    "title": "Renew JWT",
    "name": "Renew_JWT",
    "group": "Auth",
    "permission": [
      {
        "name": "token"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "access_token",
            "description": "<p>access_token.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 201": [
          {
            "group": "Success 201",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>User <code>access_token</code> to be passed to other requests.</p>"
          },
          {
            "group": "Success 201",
            "type": "Object",
            "optional": false,
            "field": "user",
            "description": "<p>Current user's data.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401",
            "description": "<p>Master access only or invalid credentials.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/api/auth/index.js",
    "groupTitle": "Auth"
  },
  {
    "type": "post",
    "url": "/devices",
    "title": "Create device",
    "group": "Devices",
    "name": "CreateDevice",
    "permission": [
      {
        "name": "master",
        "title": "Master access only",
        "description": "<p>You must pass <code>access_token</code> parameter or a Bearer Token authorization header to access this endpoint.</p>"
      }
    ],
    "success": {
      "fields": {
        "Success 201": [
          {
            "group": "Success 201",
            "type": "Device",
            "optional": false,
            "field": "device",
            "description": "<p>Device's data.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "400",
            "description": "<p>Some parameters may contain invalid values.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401",
            "description": "<p>Master access only.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/api/devices/index.js",
    "groupTitle": "Devices"
  },
  {
    "type": "js",
    "url": "device",
    "title": "Schema",
    "group": "Devices",
    "name": "Schema",
    "examples": [
      {
        "title": "Entity schema:",
        "content": "{\n  os: {\n    type: String,\n    required: true\n  },\n  token: {\n    type: String\n  },\n  locale: {\n    type: String\n  },\n  userId: {\n    type: Schema.ObjectId,\n    ref: 'User'\n  }\n};",
        "type": "js"
      }
    ],
    "version": "0.0.0",
    "filename": "src/api/devices/index.js",
    "groupTitle": "Devices"
  },
  {
    "type": "put",
    "url": "/devices/:id",
    "title": "Update device",
    "group": "Devices",
    "name": "UpdateDevice",
    "permission": [
      {
        "name": "master",
        "title": "Master access only",
        "description": "<p>You must pass <code>access_token</code> parameter or a Bearer Token authorization header to access this endpoint.</p>"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Device",
            "optional": false,
            "field": "device",
            "description": "<p>Device's data.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "400",
            "description": "<p>Some parameters may contain invalid values.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401",
            "description": "<p>Master access only.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "404",
            "description": "<p>Device not found.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/api/devices/index.js",
    "groupTitle": "Devices"
  },
  {
    "type": "get",
    "url": "/uploads",
    "title": "Retrieve uploaded files",
    "group": "FileManagement",
    "name": "RetrieveFiles",
    "permission": [
      {
        "name": "admin",
        "title": "Admin access only",
        "description": "<p>You must pass <code>access_token</code> parameter or a Bearer Token authorization header to access this endpoint.</p>"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "File[]",
            "optional": false,
            "field": "files",
            "description": "<p>List of Files.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "400",
            "description": "<p>Some parameters may contain invalid values.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401",
            "description": "<p>Admin access only.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/api/uploads/index.js",
    "groupTitle": "FileManagement",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "size": "1..30",
            "optional": true,
            "field": "page",
            "defaultValue": "1",
            "description": "<p>Page number.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "size": "1..100",
            "optional": true,
            "field": "limit",
            "defaultValue": "30",
            "description": "<p>Amount of returned items.</p>"
          },
          {
            "group": "Parameter",
            "type": "String[]",
            "optional": true,
            "field": "sort",
            "defaultValue": "-createdAt",
            "description": "<p>Order of returned items.</p>"
          },
          {
            "group": "Parameter",
            "type": "String[]",
            "optional": true,
            "field": "fields",
            "description": "<p>Fields to be returned.</p>"
          },
          {
            "group": "Parameter",
            "type": "String[fieldName]",
            "optional": true,
            "field": "singleFieldValue",
            "description": "<p>filter by element value.</p>"
          }
        ]
      }
    }
  },
  {
    "type": "post",
    "url": "/uploads",
    "title": "Upload a file",
    "group": "FileManagement",
    "name": "UploadFile",
    "permission": [
      {
        "name": "user",
        "title": "User access only",
        "description": "<p>You must pass <code>access_token</code> parameter or a Bearer Token authorization header to access this endpoint.</p>"
      }
    ],
    "parameter": {
      "fields": {
        "Body": [
          {
            "group": "Body",
            "type": "File",
            "optional": false,
            "field": "file",
            "description": "<p>The multipart-data file to be uploaded.</p>"
          },
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "metadataName",
            "description": "<p>One or more metadata to assign to the file.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "optional": false,
            "field": "201",
            "description": "<p>Created</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "400",
            "description": "<p>Some parameters may contain invalid values.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401",
            "description": "<p>user access only.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/api/uploads/index.js",
    "groupTitle": "FileManagement"
  },
  {
    "type": "post",
    "url": "/issues",
    "title": "Create issue",
    "group": "Issue",
    "name": "CreateIssue",
    "permission": [
      {
        "name": "user",
        "title": "User access only",
        "description": "<p>You must pass <code>access_token</code> parameter or a Bearer Token authorization header to access this endpoint.</p>"
      }
    ],
    "success": {
      "fields": {
        "Success 201": [
          {
            "group": "Success 201",
            "type": "Issue",
            "optional": false,
            "field": "issue",
            "description": "<p>new Issue's data.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "400",
            "description": "<p>Some parameters may contain invalid values.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/api/issues/index.js",
    "groupTitle": "Issue"
  },
  {
    "type": "delete",
    "url": "/issues/:id",
    "title": "Delete Issue",
    "group": "Issue",
    "name": "DeleteIssue",
    "permission": [
      {
        "name": "user",
        "title": "User access only",
        "description": "<p>You must pass <code>access_token</code> parameter or a Bearer Token authorization header to access this endpoint.</p>"
      }
    ],
    "success": {
      "fields": {
        "Success 204": [
          {
            "group": "Success 204",
            "optional": false,
            "field": "204",
            "description": "<p>No Content.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "404",
            "description": "<p>Issue not found.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/api/issues/index.js",
    "groupTitle": "Issue"
  },
  {
    "type": "post",
    "url": "/issues/duplicatesForChatbot",
    "title": "Retrieve issue",
    "group": "Issue",
    "name": "GetPotentialDuplicatedIssues",
    "permission": [
      {
        "name": "user",
        "title": "User access only",
        "description": "<p>You must pass <code>access_token</code> parameter or a Bearer Token authorization header to access this endpoint.</p>"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Issue",
            "optional": false,
            "field": "issue",
            "description": "<p>Issue's data.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "404",
            "description": "<p>Issue not found.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/api/issues/index.js",
    "groupTitle": "Issue"
  },
  {
    "type": "get",
    "url": "/issues/duplicates/:id",
    "title": "Retrieve issues id",
    "group": "Issue",
    "name": "GetPotentialDuplicatedIssues",
    "permission": [
      {
        "name": "user",
        "title": "User access only",
        "description": "<p>You must pass <code>access_token</code> parameter or a Bearer Token authorization header to access this endpoint.</p>"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Issue",
            "optional": false,
            "field": "issue",
            "description": "<p>Issue's data.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "404",
            "description": "<p>Issue not found.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/api/issues/index.js",
    "groupTitle": "Issue"
  },
  {
    "type": "post",
    "url": "/issues/duplicates",
    "title": "Retrieve issue",
    "group": "Issue",
    "name": "GetPotentialDuplicatedIssues",
    "permission": [
      {
        "name": "user",
        "title": "User access only",
        "description": "<p>You must pass <code>access_token</code> parameter or a Bearer Token authorization header to access this endpoint.</p>"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Issue",
            "optional": false,
            "field": "issue",
            "description": "<p>Issue's data.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "404",
            "description": "<p>Issue not found.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/api/issues/index.js",
    "groupTitle": "Issue"
  },
  {
    "type": "get",
    "url": "/issues",
    "title": "List issues",
    "group": "Issue",
    "name": "ListIssues",
    "permission": [
      {
        "name": "user",
        "title": "User access only",
        "description": "<p>You must pass <code>access_token</code> parameter or a Bearer Token authorization header to access this endpoint.</p>"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Issue[]",
            "optional": false,
            "field": "issues",
            "description": "<p>List of Issues.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/api/issues/index.js",
    "groupTitle": "Issue",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "size": "1..30",
            "optional": true,
            "field": "page",
            "defaultValue": "1",
            "description": "<p>Page number.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "size": "1..100",
            "optional": true,
            "field": "limit",
            "defaultValue": "30",
            "description": "<p>Amount of returned items.</p>"
          },
          {
            "group": "Parameter",
            "type": "String[]",
            "optional": true,
            "field": "sort",
            "defaultValue": "-createdAt",
            "description": "<p>Order of returned items.</p>"
          },
          {
            "group": "Parameter",
            "type": "String[]",
            "optional": true,
            "field": "fields",
            "description": "<p>Fields to be returned.</p>"
          },
          {
            "group": "Parameter",
            "type": "String[fieldName]",
            "optional": true,
            "field": "singleFieldValue",
            "description": "<p>filter by element value.</p>"
          }
        ]
      }
    }
  },
  {
    "type": "get",
    "url": "/issues/stats",
    "title": "List issues stats",
    "group": "Issue",
    "name": "ListIssuesStats",
    "permission": [
      {
        "name": "user",
        "title": "User access only",
        "description": "<p>You must pass <code>access_token</code> parameter or a Bearer Token authorization header to access this endpoint.</p>"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Issue[]",
            "optional": false,
            "field": "issues",
            "description": "<p>List of Issues.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/api/issues/index.js",
    "groupTitle": "Issue",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "size": "1..30",
            "optional": true,
            "field": "page",
            "defaultValue": "1",
            "description": "<p>Page number.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "size": "1..100",
            "optional": true,
            "field": "limit",
            "defaultValue": "30",
            "description": "<p>Amount of returned items.</p>"
          },
          {
            "group": "Parameter",
            "type": "String[]",
            "optional": true,
            "field": "sort",
            "defaultValue": "-createdAt",
            "description": "<p>Order of returned items.</p>"
          },
          {
            "group": "Parameter",
            "type": "String[]",
            "optional": true,
            "field": "fields",
            "description": "<p>Fields to be returned.</p>"
          },
          {
            "group": "Parameter",
            "type": "String[fieldName]",
            "optional": true,
            "field": "singleFieldValue",
            "description": "<p>filter by element value.</p>"
          }
        ]
      }
    }
  },
  {
    "type": "post",
    "url": "/near",
    "title": "Retrieve near issues by lat, long, description",
    "group": "Issue",
    "name": "Near",
    "permission": [
      {
        "name": "user",
        "title": "User access only",
        "description": "<p>You must pass <code>access_token</code> parameter or a Bearer Token authorization header to access this endpoint.</p>"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Issue",
            "optional": false,
            "field": "issue",
            "description": "<p>Issue's data.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "404",
            "description": "<p>Issue not found.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/api/issues/index.js",
    "groupTitle": "Issue"
  },
  {
    "type": "get",
    "url": "/issues/:id",
    "title": "Retrieve issue",
    "group": "Issue",
    "name": "RetrieveIssue",
    "permission": [
      {
        "name": "user",
        "title": "User access only",
        "description": "<p>You must pass <code>access_token</code> parameter or a Bearer Token authorization header to access this endpoint.</p>"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Issue",
            "optional": false,
            "field": "issue",
            "description": "<p>Issue's data.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "404",
            "description": "<p>Issue not found.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/api/issues/index.js",
    "groupTitle": "Issue"
  },
  {
    "type": "js",
    "url": "issues",
    "title": "Schema",
    "group": "Issue",
    "name": "Schema",
    "examples": [
      {
        "title": "Entity schema: */",
        "content": "  {\n    category: {\n      type: String,\n      enum: [\n        'Buca',\n        'Lampione',\n        'BlackOut',\n        'Scarico abusivo di rifiuti',\n        'Altro'\n      ]\n    },\n    coordinates: {\n      type: [Number], // [<longitude>, <latitude>]\n      index: '2dsphere',\n      default: [0, 0]\n    },\n    coordinatesEstimation: {\n      type: [Number], // [<longitude>, <latitude>]\n      index: '2dsphere',\n      default: [0, 0]\n    },\n    typedAddress: { type: String },\n    description: { type: String },\n    userInsertedKeywords: { type: [String] },\n    computatedKeywords: { type: [String] },\n    status: {\n      type: String,\n      enum: ['Aperta', 'Presa in carico', 'Risolta']\n    },\n    authorId: {\n      type: Schema.ObjectId,\n      odinVirtualPopulation: {\n        odinAutoPopulation: true,\n        fieldName: 'author',\n        options: {\n          ref: 'User'\n        }\n      }\n    },\n    source: {\n      type: String,\n      enum: ['APP', 'FACEBOOK_BOT', 'OTHER']\n    },\n    isAnonymous: {\n      type: Boolean\n    },\n    facebookIdIfAnonymous: {\n      type: String\n    },\n    chatfuel: {\n      type: Object\n    },\n    plusOnes: { type: Number },\n    pictureId: {\n      type: Schema.ObjectId,\n      odinVirtualPopulation: {\n        odinAutoPopulation: true,\n        fieldName: 'picture',\n        options: {\n          ref: 'UploadedFile'\n        }\n      }\n    },\n    facebookPictureUrl: {\n      type: String\n    },\n    latitude: {\n      type: Number,\n      odinVirtual: true\n    },\n    longitude: {\n      type: Number,\n      odinVirtual: true\n    },\n    'chatfuel user id': {\n      type: String,\n      odinVirtual: true\n    },\n    chatfuelIsAnonymous: {\n      type: String,\n      odinVirtual: true\n    },\n    image: {\n      type: String,\n      odinVirtual: true\n    },\n    'first name': {\n      type: String\n    },\n    'last name': {\n      type: String\n    },\n    note: {\n      type: String\n    }\n  };\n/*",
        "type": "js"
      }
    ],
    "version": "0.0.0",
    "filename": "src/api/issues/model.js",
    "groupTitle": "Issue"
  },
  {
    "type": "put",
    "url": "/issues/:id",
    "title": "Update issue",
    "group": "Issue",
    "name": "UpdateIssue",
    "permission": [
      {
        "name": "user",
        "title": "User access only",
        "description": "<p>You must pass <code>access_token</code> parameter or a Bearer Token authorization header to access this endpoint.</p>"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Issue",
            "optional": false,
            "field": "issue",
            "description": "<p>Issue's data.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "400",
            "description": "<p>Some parameters may contain invalid values.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "404",
            "description": "<p>Issue not found.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/api/issues/index.js",
    "groupTitle": "Issue"
  },
  {
    "type": "get",
    "url": "/notifications",
    "title": "Retrieve notifications",
    "group": "Notifications",
    "name": "RetrieveNotifications",
    "permission": [
      {
        "name": "user",
        "title": "User access only",
        "description": "<p>You must pass <code>access_token</code> parameter or a Bearer Token authorization header to access this endpoint.</p>"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "access_token",
            "description": "<p>User access_token.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "size": "1..30",
            "optional": true,
            "field": "page",
            "defaultValue": "1",
            "description": "<p>Page number.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "size": "1..100",
            "optional": true,
            "field": "limit",
            "defaultValue": "30",
            "description": "<p>Amount of returned items.</p>"
          },
          {
            "group": "Parameter",
            "type": "String[]",
            "optional": true,
            "field": "sort",
            "defaultValue": "-createdAt",
            "description": "<p>Order of returned items.</p>"
          },
          {
            "group": "Parameter",
            "type": "String[]",
            "optional": true,
            "field": "fields",
            "description": "<p>Fields to be returned.</p>"
          },
          {
            "group": "Parameter",
            "type": "String[fieldName]",
            "optional": true,
            "field": "singleFieldValue",
            "description": "<p>filter by element value.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Notification[]",
            "optional": false,
            "field": "notifications",
            "description": "<p>List of notifications.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401",
            "description": "<p>Admins access only.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/api/notifications/index.js",
    "groupTitle": "Notifications"
  },
  {
    "type": "js",
    "url": "notification",
    "title": "Schema",
    "group": "Notifications",
    "name": "Schema",
    "examples": [
      {
        "title": "Entity schema: */",
        "content": "  {\n    created_by: {\n      type: Schema.ObjectId,\n      ref: 'User'\n    },\n    targetUser: {\n      type: Schema.ObjectId,\n      ref: 'User'\n    },\n    sent: {\n      type: Boolean,\n      default: false\n    },\n    event: {\n      type: String,\n      enum: notificationTypes\n    }\n  };\n/*",
        "type": "js"
      }
    ],
    "version": "0.0.0",
    "filename": "src/api/notifications/model.js",
    "groupTitle": "Notifications"
  },
  {
    "type": "post",
    "url": "/password-resets",
    "title": "Send email",
    "name": "SendPasswordReset",
    "group": "PasswordReset",
    "permission": [
      {
        "name": "master",
        "title": "Master access only",
        "description": "<p>You must pass <code>access_token</code> parameter or a Bearer Token authorization header to access this endpoint.</p>"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Email address to receive the password reset token.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 202": [
          {
            "group": "Success 202",
            "optional": false,
            "field": "202",
            "description": "<p>Accepted.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "400",
            "description": "<p>Some parameters may contain invalid values.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/api/password-resets/index.js",
    "groupTitle": "PasswordReset"
  },
  {
    "type": "put",
    "url": "/password-resets/:token",
    "title": "Submit password",
    "name": "SubmitPasswordReset",
    "group": "PasswordReset",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "size": "6..",
            "optional": false,
            "field": "password",
            "description": "<p>User's new password.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "user",
            "description": "<p>User's data.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "400",
            "description": "<p>Some parameters may contain invalid values.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "404",
            "description": "<p>Token has expired or doesn't exist.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/api/password-resets/index.js",
    "groupTitle": "PasswordReset"
  },
  {
    "type": "get",
    "url": "/password-resets/:token",
    "title": "Verify token",
    "name": "VerifyPasswordReset",
    "group": "PasswordReset",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Password reset token.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "user",
            "description": "<p>User's data.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "404",
            "description": "<p>Token has expired or doesn't exist.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/api/password-resets/index.js",
    "groupTitle": "PasswordReset"
  },
  {
    "type": "post",
    "url": "/tickets",
    "title": "Create ticket",
    "group": "Ticket",
    "name": "CreateTicket",
    "permission": [
      {
        "name": "user",
        "title": "User access only",
        "description": "<p>You must pass <code>access_token</code> parameter or a Bearer Token authorization header to access this endpoint.</p>"
      }
    ],
    "success": {
      "fields": {
        "Success 201": [
          {
            "group": "Success 201",
            "type": "Ticket",
            "optional": false,
            "field": "ticket",
            "description": "<p>new Ticket's data.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "400",
            "description": "<p>Some parameters may contain invalid values.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/api/tickets/index.js",
    "groupTitle": "Ticket"
  },
  {
    "type": "delete",
    "url": "/tickets/:id",
    "title": "Delete Ticket",
    "group": "Ticket",
    "name": "DeleteTicket",
    "permission": [
      {
        "name": "user",
        "title": "User access only",
        "description": "<p>You must pass <code>access_token</code> parameter or a Bearer Token authorization header to access this endpoint.</p>"
      }
    ],
    "success": {
      "fields": {
        "Success 204": [
          {
            "group": "Success 204",
            "optional": false,
            "field": "204",
            "description": "<p>No Content.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "404",
            "description": "<p>Ticket not found.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/api/tickets/index.js",
    "groupTitle": "Ticket"
  },
  {
    "type": "get",
    "url": "/tickets",
    "title": "List tickets",
    "group": "Ticket",
    "name": "ListTickets",
    "permission": [
      {
        "name": "user",
        "title": "User access only",
        "description": "<p>You must pass <code>access_token</code> parameter or a Bearer Token authorization header to access this endpoint.</p>"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Ticket[]",
            "optional": false,
            "field": "tickets",
            "description": "<p>List of Tickets.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/api/tickets/index.js",
    "groupTitle": "Ticket",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "size": "1..30",
            "optional": true,
            "field": "page",
            "defaultValue": "1",
            "description": "<p>Page number.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "size": "1..100",
            "optional": true,
            "field": "limit",
            "defaultValue": "30",
            "description": "<p>Amount of returned items.</p>"
          },
          {
            "group": "Parameter",
            "type": "String[]",
            "optional": true,
            "field": "sort",
            "defaultValue": "-createdAt",
            "description": "<p>Order of returned items.</p>"
          },
          {
            "group": "Parameter",
            "type": "String[]",
            "optional": true,
            "field": "fields",
            "description": "<p>Fields to be returned.</p>"
          },
          {
            "group": "Parameter",
            "type": "String[fieldName]",
            "optional": true,
            "field": "singleFieldValue",
            "description": "<p>filter by element value.</p>"
          }
        ]
      }
    }
  },
  {
    "type": "get",
    "url": "/tickets/stats",
    "title": "List tickets stats",
    "group": "Ticket",
    "name": "ListTicketsStats",
    "permission": [
      {
        "name": "user",
        "title": "User access only",
        "description": "<p>You must pass <code>access_token</code> parameter or a Bearer Token authorization header to access this endpoint.</p>"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Ticket[]",
            "optional": false,
            "field": "tickets",
            "description": "<p>List of Tickets stats.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/api/tickets/index.js",
    "groupTitle": "Ticket",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "size": "1..30",
            "optional": true,
            "field": "page",
            "defaultValue": "1",
            "description": "<p>Page number.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "size": "1..100",
            "optional": true,
            "field": "limit",
            "defaultValue": "30",
            "description": "<p>Amount of returned items.</p>"
          },
          {
            "group": "Parameter",
            "type": "String[]",
            "optional": true,
            "field": "sort",
            "defaultValue": "-createdAt",
            "description": "<p>Order of returned items.</p>"
          },
          {
            "group": "Parameter",
            "type": "String[]",
            "optional": true,
            "field": "fields",
            "description": "<p>Fields to be returned.</p>"
          },
          {
            "group": "Parameter",
            "type": "String[fieldName]",
            "optional": true,
            "field": "singleFieldValue",
            "description": "<p>filter by element value.</p>"
          }
        ]
      }
    }
  },
  {
    "type": "get",
    "url": "/tickets/:id",
    "title": "Retrieve ticket",
    "group": "Ticket",
    "name": "RetrieveTicket",
    "permission": [
      {
        "name": "user",
        "title": "User access only",
        "description": "<p>You must pass <code>access_token</code> parameter or a Bearer Token authorization header to access this endpoint.</p>"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Ticket",
            "optional": false,
            "field": "ticket",
            "description": "<p>Ticket's data.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "404",
            "description": "<p>Ticket not found.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/api/tickets/index.js",
    "groupTitle": "Ticket"
  },
  {
    "type": "js",
    "url": "tickets",
    "title": "Schema",
    "group": "Ticket",
    "name": "Schema",
    "examples": [
      {
        "title": "Entity schema: */",
        "content": "  {\n    issueIds: {\n      type: [Schema.ObjectId],\n      odinVirtualPopulation: {\n        odinAutoPopulation: true,\n        fieldName: 'issues',\n        options: {\n          ref: 'Issue'\n        }\n      }\n    },\n    status: {\n      type: String,\n      enum: ['Nuovo', 'Preso in carico', 'Risolto']\n    },\n    description: { type: String },\n    title: { type: String },\n    executor: { type: String },\n    executionDate: { type: Date },\n    department: { type: String },\n    note: { type: String },\n    startDate: { type: Date },\n    endDate: { type: Date }\n  };\n/*",
        "type": "js"
      }
    ],
    "version": "0.0.0",
    "filename": "src/api/tickets/model.js",
    "groupTitle": "Ticket"
  },
  {
    "type": "put",
    "url": "/tickets/:id",
    "title": "Update ticket",
    "group": "Ticket",
    "name": "UpdateTicket",
    "permission": [
      {
        "name": "user",
        "title": "User access only",
        "description": "<p>You must pass <code>access_token</code> parameter or a Bearer Token authorization header to access this endpoint.</p>"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Ticket",
            "optional": false,
            "field": "ticket",
            "description": "<p>Ticket's data.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "400",
            "description": "<p>Some parameters may contain invalid values.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "404",
            "description": "<p>Ticket not found.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/api/tickets/index.js",
    "groupTitle": "Ticket"
  },
  {
    "type": "post",
    "url": "/users",
    "title": "Create user",
    "name": "CreateUser",
    "group": "User",
    "permission": [
      {
        "name": "master",
        "title": "Master access only",
        "description": "<p>You must pass <code>access_token</code> parameter or a Bearer Token authorization header to access this endpoint.</p>"
      }
    ],
    "success": {
      "fields": {
        "Sucess 201": [
          {
            "group": "Sucess 201",
            "type": "Object",
            "optional": false,
            "field": "user",
            "description": "<p>User's data.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "400",
            "description": "<p>Some parameters may contain invalid values.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401",
            "description": "<p>Master access only.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "409",
            "description": "<p>Email already registered.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/api/users/index.js",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "/users",
    "title": "Create user",
    "name": "CreateUser",
    "group": "User",
    "permission": [
      {
        "name": "master",
        "title": "Master access only",
        "description": "<p>You must pass <code>access_token</code> parameter or a Bearer Token authorization header to access this endpoint.</p>"
      }
    ],
    "success": {
      "fields": {
        "Sucess 201": [
          {
            "group": "Sucess 201",
            "type": "Object",
            "optional": false,
            "field": "user",
            "description": "<p>User's data.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "400",
            "description": "<p>Some parameters may contain invalid values.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401",
            "description": "<p>Master access only.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "409",
            "description": "<p>Email already registered.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/api/users/index.js",
    "groupTitle": "User"
  },
  {
    "type": "delete",
    "url": "/users/:id",
    "title": "Delete user",
    "name": "DeleteUser",
    "group": "User",
    "permission": [
      {
        "name": "admin",
        "title": "Admin access only",
        "description": "<p>You must pass <code>access_token</code> parameter or a Bearer Token authorization header to access this endpoint.</p>"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "access_token",
            "description": "<p>User access_token.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 204": [
          {
            "group": "Success 204",
            "optional": false,
            "field": "204",
            "description": "<p>No Content.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401",
            "description": "<p>Admin access only.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "404",
            "description": "<p>User not found.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/api/users/index.js",
    "groupTitle": "User"
  },
  {
    "type": "get",
    "url": "/users/me",
    "title": "Retrieve current user",
    "name": "RetrieveCurrentUser",
    "group": "User",
    "permission": [
      {
        "name": "user",
        "title": "User access only",
        "description": "<p>You must pass <code>access_token</code> parameter or a Bearer Token authorization header to access this endpoint.</p>"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "access_token",
            "description": "<p>User access_token.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "user",
            "description": "<p>User's data.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/api/users/index.js",
    "groupTitle": "User"
  },
  {
    "type": "get",
    "url": "/users/:id",
    "title": "Retrieve user",
    "name": "RetrieveUser",
    "group": "User",
    "permission": [
      {
        "name": "public"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "user",
            "description": "<p>User's data.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "404",
            "description": "<p>User not found.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/api/users/index.js",
    "groupTitle": "User"
  },
  {
    "type": "get",
    "url": "/users",
    "title": "Retrieve users",
    "name": "RetrieveUsers",
    "group": "User",
    "permission": [
      {
        "name": "admin",
        "title": "Admin access only",
        "description": "<p>You must pass <code>access_token</code> parameter or a Bearer Token authorization header to access this endpoint.</p>"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "access_token",
            "description": "<p>User access_token.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "size": "1..30",
            "optional": true,
            "field": "page",
            "defaultValue": "1",
            "description": "<p>Page number.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "size": "1..100",
            "optional": true,
            "field": "limit",
            "defaultValue": "30",
            "description": "<p>Amount of returned items.</p>"
          },
          {
            "group": "Parameter",
            "type": "String[]",
            "optional": true,
            "field": "sort",
            "defaultValue": "-createdAt",
            "description": "<p>Order of returned items.</p>"
          },
          {
            "group": "Parameter",
            "type": "String[]",
            "optional": true,
            "field": "fields",
            "description": "<p>Fields to be returned.</p>"
          },
          {
            "group": "Parameter",
            "type": "String[fieldName]",
            "optional": true,
            "field": "singleFieldValue",
            "description": "<p>filter by element value.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "users",
            "description": "<p>List of users.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "400",
            "description": "<p>Some parameters may contain invalid values.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401",
            "description": "<p>Admin access only.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/api/users/index.js",
    "groupTitle": "User"
  },
  {
    "type": "js",
    "url": "users",
    "title": "Schema",
    "name": "Schema",
    "group": "User",
    "examples": [
      {
        "title": "Entity schema: */",
        "content": "{\n  email: {\n    type: String,\n    match: /^\\S+@\\S+\\.\\S+$/,\n    required: true,\n    unique: true,\n    trim: true,\n    lowercase: true,\n    odinFrozenAfterCreation: true\n  },\n  password: {\n    type: String,\n    required: true,\n    minlength: 6\n  },\n  role: {\n    type: String,\n    enum: roles,\n    default: 'app-user',\n    odinFrozenAfterCreation: true\n  },\n  pictureId: {\n    type: Schema.ObjectId\n  },\n  isConfirmed: {\n    type: Boolean,\n    default: false\n  },\n  experiencePoints: {\n    type: Number\n  },\n  badges: {\n    type: [String] // tipo achivement per l'utente\n  },\n  name: {\n    type: String\n  },\n  facebookId: {\n    type: String\n  },\n  department: {\n    type: String,\n    enum: [\n      'Lavori pubblici',\n      'Innovazione',\n      'Sport',\n      'Turismo',\n      'Politiche per lo sviluppo',\n      'Pianificazione urbanistica'\n    ]\n  },\n  last_login: {\n    // last real login\n    type: Date\n  },\n  pre_last_login: {\n    // last login showed to the user\n    type: Date\n  }\n};\n/*",
        "type": "js"
      }
    ],
    "version": "0.0.0",
    "filename": "src/api/users/model.js",
    "groupTitle": "User"
  },
  {
    "type": "put",
    "url": "/users/:id/password",
    "title": "Update password",
    "name": "UpdatePassword",
    "group": "User",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>Basic authorization with email and password.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 201": [
          {
            "group": "Success 201",
            "type": "Object",
            "optional": false,
            "field": "user",
            "description": "<p>User's data.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "400",
            "description": "<p>Some parameters may contain invalid values.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401",
            "description": "<p>Current user access only.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "404",
            "description": "<p>User not found.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/api/users/index.js",
    "groupTitle": "User"
  },
  {
    "type": "put",
    "url": "/users/:id",
    "title": "Update user",
    "name": "UpdateUser",
    "group": "User",
    "permission": [
      {
        "name": "user",
        "title": "User access only",
        "description": "<p>You must pass <code>access_token</code> parameter or a Bearer Token authorization header to access this endpoint.</p>"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "user",
            "description": "<p>User's data.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "400",
            "description": "<p>Some parameters may contain invalid values.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401",
            "description": "<p>Current user or admin access only.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "404",
            "description": "<p>User not found.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/api/users/index.js",
    "groupTitle": "User"
  }
] });
